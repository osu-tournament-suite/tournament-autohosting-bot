from time import sleep

import requests
from osuapi import OsuApi, ReqConnector

from osubot.CONFIG import config
from osubot.data import RoundData


class ApiBridge:
    _Instance = None

    def __init__(self):
        ApiBridge._Instance = self
        self.osu_api = OsuApi(config.OSU_API_KEY, connector=ReqConnector())

    @staticmethod
    def get_instance():
        if ApiBridge._Instance is None:
            ApiBridge()
        return ApiBridge._Instance

    def round_data_from_uuid(self, uuid):
        def wait3(uuid):
            sleeptime = 0
            request = requests.get(
                "http://www.osutournaments.com:8080/api/v1/get_round?k=" + config.API_KEY + "&uuid=" + uuid)
            while 'error' in request.json():
                request = requests.get(
                    "http://www.osutournaments.com:8080/api/v1/get_round?k=" + config.API_KEY + "&uuid=" + uuid)
                json = request.json()
                if 'error' in json:
                    pass
                else:
                    return json
                sleeptime += 5
                sleep(sleeptime)
            return request.json()

        json = wait3(uuid)
        return RoundData(json["maps"], json["name"], [], json["refs"], json["extras"])
    def team_data_from_uuid(self, uuid):
        def wait3(uuid):
            sleeptime = 0
            request = requests.get(
                "http://www.osutournaments.com:8080/api/v1/get_team?k=" + config.API_KEY + "&uuid=" + uuid)
            while 'error' in request.json():
                request = requests.get(
                    "http://www.osutournaments.com:8080/api/v1/get_team?k=" + config.API_KEY + "&uuid=" + uuid)
                json = request.json()
                if 'error' in json:
                    pass
                else:
                    return json
                sleeptime += 5
                sleep(sleeptime)
            return request.json()

        json = wait3(uuid)
        return json
    def get_scores(self, mp_id):
        return self.osu_api.get_match(mp_id)

    def push_finished_round(self, id, time, name, maps, refs, scores, teams, extras):
        pass
        # TODO: IMPLEMENT THIS
