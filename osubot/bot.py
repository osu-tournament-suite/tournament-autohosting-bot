import logging
import re
from time import monotonic as time
from time import sleep

import irc.client

from osubot.CONFIG import config
from osubot.game_manger import GameManger

_logger = logging.getLogger(__name__)


class Core:
    """"Core osu bot class that handles connecting to irc and processing commands"""

    _Instance = None

    def __init__(self):
        self.reader = None
        self.writer = None
        self.command_queue = []
        self.IRC = None
        self.match_queue = []
        Core._Instance = self

    @staticmethod
    def get_instance():
        if not Core._Instance:
            Core()
        return Core._Instance

    def on_message(self, connection, message):
        """Fired when a message is received over IRC"""
        result = re.search("https://osu\.ppy\.sh/mp/([0-9]+)", message.arguments[0])
        if result is not None:
            _logger.info("Joining room mp_" + result.group(1))
            self.IRC.join("mp_" + result.group(1))

            print(self.match_queue[0]['round_uuid'])

            new_game_manager = GameManger(self, self.match_queue[0]['match_uuid'], result.group(1),
                                          self.match_queue[0]['round_uuid'],
                                          self.match_queue[0]['team_uuids'])
            new_game_manager.start()

            self.match_queue.pop(0)

        if message.arguments[0] == "The match has finished!":
            channel = re.search("#mp_([0-9]+)", message.target).group(1)
            self.command_queue.append({'context': channel, 'command': {'command': 'finished'}})

        if message.arguments[0] == "The match has been aborted":
            channel = re.search("#mp_([0-9]+)", message.target).group(1)
            self.send_message(channel, "Warning: A ref has aborted the match")

        if message.arguments[0] == "All players are ready":
            channel = re.search("#mp_([0-9]+)", message.target).group(1)
            self.command_queue.append({'context': channel, 'command': {'command': 'ready'}})

        if re.search("([\w\s]+) joined in slot", message.arguments[0]) is not None:
            channel = re.search("#mp_([0-9]+)", message.target).group(1)
            user = re.search("([\w\s]+) joined in slot", message.arguments[0]).group(1)
            user = user.replace(" ", "_")
            print(user + " joined")
            self.command_queue.append({'context': channel, 'command': {'command': 'join', 'user': user}})

        if re.search("([\w\s]+) left the game.", message.arguments[0]) is not None:
            channel = re.search("#mp_([0-9]+)", message.target).group(1)
            user = re.search("([\w\s]+) left the game", message.arguments[0]).group(1)
            user = user.replace(" ", "_")
            print(user + " left")
            self.command_queue.append({'context': channel, 'command': {'command': 'leave', 'user': user}})

        if message.arguments[0].startswith("!"):
            channel = re.search("#mp_([0-9]+)", message.target).group(1)
            command = message.arguments[0][1:]
            command = command.split(" ")
            user = message.source.split("!")[0]
            if len(command) > 1:
                self.command_queue.append({'context': channel, 'command': {'command': command[0], 'args': command[1],
                                                                           'user': user}})
            else:
                self.command_queue.append({'context': channel, 'command': {'command': command[0], 'args': "",
                                                                           'user': user}})

    def start_client(self):
        reactor = irc.client.Reactor()

        self.IRC = reactor.server().connect("irc.ppy.sh", 6667, config.OSU_IRC_USERNAME,
                                            password=config.OSU_IRC_PASSWORD)

        self.IRC.add_global_handler("privmsg", self.on_message)  # Used for PM's
        self.IRC.add_global_handler("pubmsg", self.on_message)  # Used for channel messages

        reactor.process_forever()

    def join(self, mp_room):
        self.IRC.join("mp_" + mp_room)

    def pull_command(self, mp_room, timeout):
        """Pulls a command or waits until the timeout has been reached"""
        result = None
        endtime = time() + timeout

        while not result:
            remaining = endtime - time()
            if remaining <= 0.0:
                return None;
            for command in self.command_queue:
                if command["context"] == mp_room:
                    print("owo")
                    self.command_queue.remove(command)
                    return command["command"]

    def close_room(self, mp_room):
        self.send_message(mp_room, "!mp close")

    def send_message(self, mp_room, message):
        """Sends a message to the multiplayer room with the id specified"""
        sleep(1)  # This is to prevent the bot account from getting muted during testing, remove for production
        print("Sending message " + message + " to room " + mp_room)
        self.IRC.privmsg("#mp_"+str(mp_room), message)

    def join_room(self, mp_room):
        """Joins the IRC channel of an osu multiplayer room"""

        self.join("#mp_" + mp_room)

    def make_room(self, match_uuid, team_uuids, round_uuid):
        """Makes an osu multiplayer room"""

        # Add the match to the match_queue
        self.match_queue.append({'match_uuid': match_uuid, 'round_uuid': round_uuid, 'team_uuids': team_uuids})

        self.IRC.privmsg("BanchoBot", "!mp make room")  # Tell Bancho to make the bot a room
