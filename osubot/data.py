class RoundData:
    def __init__(self, maps, tournament_name, team_uuids, ref_usernames, extras):
        self.maps = maps
        self.tournament_name = tournament_name
        self.team_uuids = team_uuids
        self.ref_usernames = ref_usernames
        self.extras = extras

class MapList:
    def __init(self, maps):
        self.maps = maps