import logging
import random
import threading
from time import sleep

from math import floor

from osubot.api_bridge import ApiBridge
from osubot.message_queue import MessageQueue


class GameManger(threading.Thread):

    def __init__(self, manager, match_uuid, mp_room_id, round_uuid, team_uuids):

        self.core_manager = manager  # Used to get information about the room we're in and query other stuff
        self.mp_room_id = mp_room_id  # Used to identify what mp room this game manager is managing
        self.round_uuid = round_uuid  # Used to get round information from the api
        self.team_uuids = team_uuids  # Team uuid to make requests from the api
        self.round_data = None  # This is later redefined as an array of round-datas
        self.team_data = []  # Stores the data for both teams
        self.closed = False  # Is the match closed
        self._logger = logging.getLogger(__name__)  # The logger to log to
        self.grab_information()  # Load all the information from the team_uuids and round_uuid
        self.joined_users = []  # The users that are currently in the lobby (used for team validation)
        self.match_uuid = match_uuid  # The UUID of the match that is being ran
        self.message_queue = MessageQueue.get_instance()  # The message queue

        threading.Thread.__init__(self)  # Init thread stuff

    def grab_information(self):
        """Grabs information from the ApiBridge for this match"""

        api_bridge = ApiBridge.get_instance()
        self.round_data = api_bridge.round_data_from_uuid(self.round_uuid)  # Load up the round data

        for team in self.team_uuids:
            self.team_data.append(api_bridge.team_data_from_uuid(team))  # Guess what, we're loading the team data

    def pull_command_wrapper(self, timeout):
        """A wrapper for pulling commands so that there are global commands"""
        command = self.core_manager.pull_command(self.mp_room_id, timeout)  # Pull a command

        def verify_user(user):
            """Verifies a user is actually supposed to be in this room"""
            for team in self.team_data:
                for player in team["players"]:
                    if player["name"] == user:
                        return True
            return False

        if command is not None and command["command"] == "join":  # Fired when a user joins the lobby
            if verify_user(command["user"]):
                for i in range(0, len(self.team_data)):
                    print(self.team_data[i])
                    for user in self.team_data[i]["players"]:
                        if user["name"] == command["user"]:
                            print("Adding user: " + user["name"])
                            self.joined_users.append(user)
                            if i == 0:
                                self.message_queue.queue_message(self.mp_room_id, "!mp team " + user["name"] + " blue")
                            else:
                                self.message_queue.queue_message(self.mp_room_id, "!mp team " + user["name"] + " red")
            else:
                self.message_queue.queue_message(self.mp_room_id, "!mp kick " + command["user"])
            command = None  # Set it to none

        if command is not None and command[
            "command"] == "leave":  # Fired when a user exits the lobby (in game only not irc)
            for user in self.joined_users:
                if user["name"] == command["user"]:
                    print('Removing user: ' + user["name"])
                    self.joined_users.remove(user)
            command = None  #

        if command is not None and command["command"] == "list":  # Lists the maps in the pool
            message = "Available Maps:"
            for map in self.round_data.maps:
                message += " " + map["type"]
            self.message_queue.queue_message(self.mp_room_id, message)
            command = None

        if command is not None and command["command"] == "ref":  # Calls a ref
            for ref in self.round_data.ref_usernames:
                self.core_manager.IRC.privmsg(ref, "A lobby has requested your help!")
                self.message_queue.queue_message(self.mp_room_id, "!mp invite " + ref)
                self.message_queue.queue_message(self.mp_room_id, "!mp size 5")
            command = None

        return command  # Return the commmand, or None if it was a global command / event
    
    def check_if_captain(self, user):
        """Checks if a user is a captain of either team"""
        for team in self.team_data:
            for captain in team["captains"]:
                if user == captain:
                    return True  # User is a captain
        return False  # If the user has not already been found in the captains db then just return false

    @staticmethod
    def check_if_on_team(team, user):
        """Checks if a player is on the team specified"""
        for player in team["players"]:
            if player["name"] == user:
                return True  # Player is on team
        return False  # Player is not on team

    def run(self):
        try:
            print(self.team_data)
            if self.round_data.extras["type"] is not 0:  # This is a tournament match and should be logged as such
                print("Starting Game Manager for " + self.round_data.tournament_name + " round " + str(
                    self.round_uuid) + ": " +
                      self.team_data[0]["name"] + " VS " + self.team_data[1]["name"])
            else:  # This is a qualifier match
                print("Starting Game Manager for team " + self.team_data[0]["name"] + 's qualifiers')

            run = False  # When this is true the game is ready to be started

            self.message_queue.queue_message(self.mp_room_id, "!mp set 2 3 4")  # Configure the default room settings

            for ref in self.round_data.ref_usernames:
                self.message_queue.queue_message(self.mp_room_id,
                                                 "!mp addref " + ref)  # Add refs as refs in case of error

            for team in self.team_data:
                for player in team["players"]:
                    print(team["players"])
                    self.message_queue.queue_message(self.mp_room_id,
                                                     "!mp invite " + player["name"])  # Invite all players

            # If this is a tournament match
            while not self.closed and self.round_data.extras["type"] == 1:

                def start(user):
                    """Checks if the game should be started based on inputted user"""
                    if self.check_if_captain(user):  # Only start if the user issuing !start is a captain
                        self.message_queue.queue_message(self.mp_room_id, "Starting round!")
                        return True
                    self.message_queue.queue_message(self.mp_room_id, "A captain must initiate the round!")

                while not run:
                    command = self.pull_command_wrapper(60)  # TODO: Find better timeout for this

                    if command is None:
                        # Send the players a hint in case they forgot the command somehow
                        self.message_queue.queue_message(self.mp_room_id,
                                                         "Please have a captain execute !start when both teams are ready!")

                    # Check if the command that was pulled was the start command, which is the only valid command
                    if command is not None and command["command"] == "start":
                        if start(command["user"]):  # If the round should be started
                            run = True  # The round is ready to start

                while run:

                    def is_picked(map):
                        for map in picked_maps:
                            if map["type"] == map:
                                return True
                        return False

                    api_bridge = ApiBridge.get_instance()
                    picked_maps = []  # Maps that have been previously picked and cannot be picked again
                    banned_maps = []  # Maps that are banned
                    banning = True
                    playing = True
                    ordered_team = [None,
                                    None]  # The two team objects, in the order they are picked based on "coin toss"
                    round_max = self.round_data.extras[
                        "best_of"]  # The max amount of rounds that should be played, tiebreakers are unsupported
                    round_results = []  # Round results

                    # Send a thank you message to the players :)
                    self.message_queue.queue_message(self.mp_room_id, "Starting game! Thanks for using osu!autohost :)")

                    ordered_team[0] = self.team_data[random.randint(0, 1)]  # Pick a random team to be first

                    for team in self.team_data:  # Set the non-first team to the second slot
                        if ordered_team[0] is not team:
                            ordered_team[1] = team

                    sleep(2)  # Sleep for two seconds to not spam chat as much

                    self.message_queue.queue_message(self.mp_room_id, "Team " + ordered_team[0]["name"] +
                                                     " has won the coin toss and will be banning first")

                    while banning:
                        chosen_map = None
                        for team in ordered_team:
                            self.message_queue.queue_message(self.mp_room_id, team[
                                "name"] + " is banning, have a captain use !ban [map] to ban a map!")
                            while True:
                                command = self.pull_command_wrapper(20)

                                def is_banned(map):
                                    if self.check_if_captain(command["user"]):

                                        for map in banned_maps:
                                            if map["type"] == command["args"]:
                                                # Map was already banned and should not be banned again
                                                self.message_queue.queue_message(self.mp_room_id,
                                                                                 "Map already banned!"
                                                                                 " Please pick a different map.")
                                                return True  # The map was already banned so return True
                                    else:
                                        self.message_queue.queue_message(self.mp_room_id,
                                                                         "Only a captain can ban maps!")
                                    return False  # The map is not banned or the user who called the command is not captain

                                def ban_map(map):
                                    for map in self.round_data.maps:
                                        if map["type"] == command["args"]:
                                            self.message_queue.queue_message(self.mp_room_id, "Banning " + map["type"])
                                            # Add the map that was just banned to the banned maps list
                                            banned_maps.append(map)
                                            return True

                                    self.message_queue.queue_message(self.mp_room_id,
                                                                     "Map not found in pool, please pick a different map!")
                                    return False

                                # Banning logic
                                if command is not None and command["command"] == "ban":
                                    if not is_banned(command["args"]):  # If the map inst already banned
                                        if self.check_if_on_team(team, command["user"]) \
                                                and self.check_if_captain(command["user"]):

                                            if ban_map(command["args"]):  # Ban the map
                                                break  # Break out of the while loop

                                        else:

                                            self.message_queue.queue_message(self.mp_room_id,
                                                                             "!ban must be executed by "
                                                                             "a captain on the choosing team")

                        # Output banned maps
                        message = "Maps:"

                        for map in banned_maps:
                            message += " " + map["type"]
                        message += " have been banned."
                        self.message_queue.queue_message(self.mp_room_id, message)

                        banning = False  # End banning

                    while playing:
                        for team in ordered_team:
                            team_1_wins = 0
                            team_2_wins = 0
                            for round in round_results:

                                print(str(round[0]) + " points vs " + str(round[1]) + "points")

                                if round[0] > round[1]:
                                    team_1_wins += 1
                                else:
                                    team_2_wins += 1

                                if team_1_wins > floor(round_max / 2) or team_2_wins > floor(round_max / 2):
                                    playing = False
                                    break

                            print(str(team_1_wins) + " wins vs " + str(team_2_wins) + " wins")

                            if playing is False:
                                break

                            self.message_queue.queue_message(self.mp_room_id,
                                                             "Its time to choose a map! " + team[
                                                                 "name"] + " will be choosing!")

                            picked_map = None

                            while not picked_map:

                                self.message_queue.queue_message(self.mp_room_id,
                                                                 "Use !map [map] to choose a map")
                                command = self.pull_command_wrapper(20)

                                if command is not None and command["command"] == "map":

                                    if self.check_if_captain(command["user"]) and self.check_if_on_team(team,
                                                                                                        command[
                                                                                                            "user"]):
                                        if not is_banned(command["args"]) and not is_picked(command["args"]):

                                            for map in self.round_data.maps:  # Choose the map

                                                if map["type"] == command["args"]:
                                                    self.message_queue.queue_message(self.mp_room_id,
                                                                                     "Picking " + map["type"])

                                                    picked_map = map
                                                    picked_maps.append(map)
                                        else:
                                            self.message_queue.queue_message(self.mp_room_id,
                                                                             "That map is banned or has already been picked!")
                                    else:

                                        self.message_queue.queue_message(self.mp_room_id,
                                                                         "Only a member of the team who is a captain can do that!")

                            ready = False

                            self.message_queue.queue_message(self.mp_room_id, "!mp map " + picked_map["id"])
                            self.message_queue.queue_message(self.mp_room_id, "!mp mods " + picked_map["mods"])

                            while not ready:
                                self.message_queue.queue_message(self.mp_room_id, "Everyone ready up!")
                                command = self.pull_command_wrapper(20)
                                print(command)
                                if command is not None and command["command"] == "ready":
                                    break

                            self.message_queue.queue_message(self.mp_room_id, "!mp start 15")

                            while picked_map:

                                # Have a timeout so it can print out a hint
                                command = self.pull_command_wrapper(20)

                                if command is not None and command["command"] == "finished":
                                    sleep(1)
                                    scores = api_bridge.get_scores(self.mp_room_id)
                                    while scores is None:
                                        sleep(1)
                                        scores = api_bridge.get_scores(self.mp_room_id)

                                    round_results.append([])
                                    team_1_score = 0
                                    team_2_score = 0
                                    for score in scores.games[len(round_results) - 1].scores:
                                        for i in range(0, 2):
                                            for player in self.team_data[i]["players"]:
                                                if str(score.user_id) == player["id"]:
                                                    if i == 0:
                                                        team_1_score += score.score
                                                    if i == 1:
                                                        team_2_score += score.score

                                    round_results[len(round_results) - 1] = [team_1_score, team_2_score]

                                    if team_1_score == team_2_score:
                                        self.message_queue.queue_message(self.mp_room_id, "Congrats on tying!")

                                    if team_1_score > team_2_score:
                                        self.message_queue.queue_message(self.mp_room_id, "Congrats to team " +
                                                                         self.team_data[0]["name"] +
                                                                         "  for winning with " + str(
                                            team_1_score) + " points")
                                    else:
                                        self.message_queue.queue_message(self.mp_room_id, "Congrats to team " +
                                                                         self.team_data[1]["name"] +
                                                                         "  for winning with " + str(
                                            team_2_score) + " points")
                                    picked_map = None

                    team_1_wins = 0
                    team_2_wins = 0

                    for round in round_results:

                        if round[0] > round[1]:
                            team_1_wins += 1
                        else:
                            team_2_wins += 1

                    if team_1_wins > team_2_wins:
                        self.message_queue.queue_message(self.mp_room_id,
                                                         "The match has ended, " + self.team_data[0][
                                                             "name"] + " wins! Thanks for playing!")
                    else:
                        self.message_queue.queue_message(self.mp_room_id,
                                                         "The match has ended, " + self.team_data[1][
                                                             "name"] + " wins! Thanks for playing!")

                    api_bridge.push_finished_round(self.round_uuid, 0, self.round_data.tournament_name,
                                                   self.round_data.maps, self.round_data.ref_usernames, round_results,
                                                   self.team_uuids, self.round_data.extras)

                    sleep(10)

                    self.message_queue.queue_message(self.mp_room_id, "This room will close in 30 seconds!")

                    sleep(30)

                    self.core_manager.close_room(self.mp_room_id)  # Close the room after the round is over

                    run = False

                    self.closed = True

                    break

            # If this is a qualifier match
            while not self.closed and self.round_data.extras["type"] == 0:

                def start(user):
                    """Checks if qualifiers should be started based on inputted user"""
                    if self.check_if_captain(user):  # Only start if the user issuing !start is a captain
                        self.message_queue.queue_message(self.mp_room_id, "Starting round!")
                        return True
                    self.message_queue.queue_message(self.mp_room_id, "A captain must initiate qualifiers.")

                while not run:
                    command = self.pull_command_wrapper(30)

                    if command is None:
                        # Send the players a hint in case they forgot the command somehow
                        self.message_queue.queue_message(self.mp_room_id,
                                                         "Please have the captain execute !start when your teams are ready!")

                    # Check if the command that was pulled was the start command, which is the only valid command
                    if command is not None and command["command"] == "start":
                        if start(command["user"]):  # If the round should be started
                            run = True  # The round is ready to start

                api_bridge = ApiBridge.get_instance()
                round_results = []

                for map in self.round_data.maps:

                    self.message_queue.queue_message(self.mp_room_id, "It's time to play " + map[
                        "type"] + " the map will start when everyone is ready!")

                    self.message_queue.queue_message(self.mp_room_id, "!mp map " + map["id"])
                    self.message_queue.queue_message(self.mp_room_id, "!mp mods " + map["mods"])

                    ready = False

                    while not ready:
                        command = self.pull_command_wrapper(20)
                        print(command)
                        if command is not None and command["command"] == "ready":
                            break

                    self.message_queue.queue_message(self.mp_room_id, "!mp start")

                    while True:
                        command = self.pull_command_wrapper(20)

                        if command is not None and command["command"] == "finished":
                            sleep(1)
                            scores = api_bridge.get_scores(self.mp_room_id)
                            while scores is None:
                                sleep(1)
                                scores = api_bridge.get_scores(self.mp_room_id)

                            round_results.append([])
                            team_1_score = 0
                            for score in scores.games[len(round_results) - 1].scores:
                                team_1_score += score.score
                            self.message_queue.queue_message(self.mp_room_id,
                                                             "You scored " + str(team_1_score) + " on map "
                                                             + map["type"])
                            round_results[len(round_results) - 1] = [team_1_score]
                            break

                self.closed = True

                api_bridge.push_finished_round(self.match_uuid, 0, self.round_data.tournament_name,
                                               self.round_data.maps, self.round_data.ref_usernames, round_results,
                                               self.team_uuids, self.round_data.extras)
                self.message_queue.queue_message(self.mp_room_id, "!mp close")
        except Exception as e:
            print(e)
            self.message_queue.queue_message(self.mp_room_id,
                                             "WARNING: AN ERROR HAS OCCURED, PLEASE REPORT THIS TO THE DEVS ALL COMMANDS AND MATCH MANAGEMENT WILL NOW CEASE")
            self.message_queue.queue_message(self.mp_room_id, "PLEASE MANUALLY CONTACT A REF TO CONTINUE THE ROUND")
            self.message_queue.queue_message(self.mp_room_id)
