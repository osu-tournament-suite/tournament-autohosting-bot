import threading
from time import sleep


class MessageQueue(threading.Thread):
    def setup(self):
        from osubot.bot import Core
        self.core_bot = Core.get_instance()

    _Instance = None

    queue = []

    def __init__(self):
        MessageQueue._Instance = self
        threading.Thread.__init__(self)
        self.setup()

    @staticmethod
    def get_instance():
        if MessageQueue._Instance is None:
            MessageQueue()
        return MessageQueue._Instance

    def queue_message(self, room, message):
        self.queue.append({'room': room, 'message': message})

    def run(self):
        while True:
            sleep(.1)
            for message in self.queue:
                sleep(.2)
                self.core_bot.send_message(message['room'], message['message'])
                self.queue.remove(message)
