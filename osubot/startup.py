import db_interface.core
from osubot.bot import Core
from osubot.message_queue import MessageQueue

message_queue = MessageQueue.get_instance()
bot = Core.get_instance()
db_core = db_interface.core.Db_Core()

message_queue.start()
db_core.start()
bot.start_client()
